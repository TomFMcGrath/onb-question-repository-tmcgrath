(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
25 RESTful API Testing with Postman
26 Experience Design Patterns in Java
27 Java Web Services
28 Windows Tips & Tricks
29 The Internet & World Wide Web
29 Excel
30 Java Spring Tutorial

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Tsetsen erdene Ganbaatar
(Course Site): discover-devtools.codeschool.com
(Course Name): Explore and Master Chrome DevTools
(Course URL): http://discover-devtools.codeschool.com/
(Discipline): Technical
(ENDIGNORE)

(Type): truefalse
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): JPEG is a good file format to use for icon images
(A): True
(B): False
(Correct): B
(Points): 1
(CF): PNGs are a much more efficient image format for icons.
(WF): PNGs are a much more efficient image format for icons.
(STARTIGNORE)
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): Hitting Ctrl-S in the Source mode does what?
(A): Save the source file to the browser session.
(B): Save the source file to local storage.
(C): Save the source file to Google Drive.
(D): Save the source file to the request.
(E): Brings up a menu where you can select where to save the file to.
(Correct): A
(Points): 1
(CF): Ctrl-S saves the file to the browser session meaning that it persists for page refreshes and such but these changes don't affect the local project files. Use "Save as" to do that.
(WF): Ctrl-S saves the file to the browser session meaning that it persists for page refreshes and such but these changes don't affect the local project files. Use "Save as" to do that.
(STARTIGNORE)
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): A JavaScript error occurs during page load and the exception stack trace is shown in the console. How can we easily further investigate this error?
(A): Refresh the page to see if the error arises again.
(B): Read over the javascript locally and fix it from there.
(C): Click on the line number next to the error. DevTools will open the line that caused the error in Source mode.
(D): Switch to the Application view and look at the cache storage.
(E): Turn on "Pause on Exception" and reload the page.
(Correct): C
(Points): 1
(CF): The DevTools Console neatly displays errors in red and can smartly link to the exact line that caused that error in Source mode.
(WF): The DevTools Console neatly displays errors in red and can smartly link to the exact line that caused that error in Source mode.
(STARTIGNORE)
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Ryan Stacy
(Course Site): Lynda
(Course Name): HTML Essentials	
(Course URL): http://www.lynda.com/HTML-tutorials/HTML-Essential-Training-2012/99326-2.html
(Discipline): HTML
(ENDIGNORE)

(Type): multiplechoice
(Category): 8
(Grade style): 0
(Random answers): 1
(Question): What is the mode for supporting old version of HTML usually called in browsers? 
(A): Quirks mode
(B): Legacy mode
(C): Deprecated mode
(D): Backwards Compatibility Mode
(Correct): A
(Points): 1
(CF): Legacy HTML support is usually referred to as quirks mode and will support almost anything that looks like HTML, but it will look different in different browsers.
(WF): Legacy HTML support is usually referred to as quirks mode and will support almost anything that looks like HTML, but it will look different in different browsers.
(STARTIGNORE)
(Hint): 
(Subject): HTML	
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 8
(Grade style): 0
(Random answers): 1
(Question): What happens when you link a CSS file that does not exist in the <head> of an HTML document? 
(A): You will get a 404 error
(B): Nothing will display
(C): The page with be styled with browser defaults
(D): Only the title from the head with display
(Correct): C
(Points): 1
(CF): The browser will simply render the page with its own styles if the CSS link is not found.  A viewer will not see an error displayed.
(WF): The browser will simply render the page with its own styles if the CSS link is not found.  A viewer will not see an error displayed.
(STARTIGNORE)
(Hint):
(Subject): HTML
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 8
(Grade style): 1
(Random answers): 1
(Question): Which of the following are valid ways to set the Character Set in a meta tag? 
(A): charset="UTF-8"
(B): charset=UTF-8
(C): characterset="UTF-8"
(D): charset="UTF-8'
(Correct): A, B
(Points): 1
(CF): You cannot end a double quote with a single quote. The quotation marks are optional in HTML, both with and without will work. Note: using quotes will make your code Polyglot; valid in both HTML and XHTML. 
(WF): You cannot end a double quote with a single quote. The quotation marks are optional in HTML, both with and without will work. Note: using quotes will make your code Polyglot; valid in both HTML and XHTML. 
(STARTIGNORE)
(Hint): 
(Subject): HTML
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 8
(Grade style): 0
(Random answers): 1
(Question): HTML5 is closely tied to SGML (Standard Generalized Markup Language)
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Recent versions of HTML are no longer tied to SGML.
(WF): Recent versions of HTML are no longer tied to SGML.
(STARTIGNORE)
(Hint):
(Subject): HTML
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 8
(Grade style): 1
(Random answers): 1
(Question): Which of the following are valid uses for the meta tag? (Select all that apply)
(A): Applying a character set
(B): Simulate HTTP headers
(C): Forward to another page
(D): To Write Javascript
(Correct): A, B, C
(Points): 1
(CF): Javascript goes in script tags.  You can simulate an HTTP header with the meta tag, and by extension of that you can forward the user to another page.
(WF): Javascript goes in script tags.  You can simulate an HTTP header with the meta tag, and by extension of that you can forward the user to another page.
(STARTIGNORE)
(Hint):
(Subject): HTML
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 8
(Grade style): 0
(Random answers): 1
(Question): The alt tag and title tag are identical for images.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): The alt tag is what will display when the image can't be loaded for whatever reason.  The title tag is what appears when you hover over the image.
(WF): The alt tag is what will display when the image can't be loaded for whatever reason.  The title tag is what appears when you hover over the image.
(STARTIGNORE)
(Hint): 
(Subject): HTML
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 8
(Grade style): 0
(Random answers): 1
(Question): Which of the following are valid HTML tags for lists?
(A): <ul>
(B): <ol>
(C): <dl>
(D): <fl>
(Correct): A, B, C
(Points): 1
(CF): <fl> is not a list tag. <ul> is an unordered list using bullets, <ol> is an ordered list using numbers, <dl> is a definition list.
(WF): <fl> is not a list tag. <ul> is an unordered list using bullets, <ol> is an ordered list using numbers, <dl> is a definition list.
(STARTIGNORE)
(Hint): 
(Subject): HTML
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 8
(Grade style): 0
(Random answers): 1
(Question): Why might it be a good idea to specify the width and height of an image tag?
(A): So the user can't zoom
(B): So it will always display correctly
(C): To prevent the image from moving
(D): To help slow loading paging orient content before the image is loaded
(Correct): D
(Points): 1
(CF): Without the size specification, a slow loading page might jump text or links around after the browser finds out the size of the image.  Specifying size helps the browser designate space for it.
(WF): Without the size specification, a slow loading page might jump text or links around after the browser finds out the size of the image.  Specifying size helps the browser designate space for it.
(STARTIGNORE)
(Hint):
(Subject): HTML
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 8
(Grade style): 0
(Random answers): 1
(Question): True or False - You cannot go up a directory using relative URLs.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): You can use ".." to go to the above directory.
(WF): You can use ".." to go to the above directory.
(STARTIGNORE)
(Hint):
(Subject): HTML
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 8
(Grade style): 0
(Random answers): 1
(Question): What is true about the semantic tags in HTML5?
(A): They are meant to reinforce meaning
(B): They manipulate the presentation
(C): They change the content
(D): They are not yet supported
(Correct): A
(Points): 1
(CF): Semantic tags such as <article> or <footer> infer meaning to content.  They function the same as a <div>, do they aren't changing content or presentation.  They are quite supported in modern browsers.
(WF): Semantic tags such as <article> or <footer> infer meaning to content.  They function the same as a <div>, do they aren't changing content or presentation.  They are quite supported in modern browsers.
(STARTIGNORE)
(Hint): 
(Subject): HTML
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Joseph Duda
(Course Site): Lynda.com
(Course Name): Java Essentials Training
(Course URL) : http://www.lynda.com/Java-tutorials/Java-Essential-Training/377484-2.html
(Discipline): Technical 
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Which of the following can Java be used to program?
(A): Phone SIM cards.
(B): Enterprise applications.
(C): Android applications.
(D): iPhone/iOS applications.
(E): Linux applications.
(Correct): A, B, C, E
(Points): 1
(CF): Java is very versatile and can be used to program many things. However, iPhone/iOS applications are written using Objective C.
(WF): Java is very versatile and can be used to program many things. However, iPhone/iOS applications are written using Objective C.
(STARTIGNORE)
(Hint):
(Subject): Java Essentials Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 1 
(Grade style): 0
(Random answers): 1
(Question): In Java you can dereference primitives and objects by assigning them to null.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Setting to null will allow the garbage collector the option of removing them from memory. It is up to the garbage collector whether it does so or not.
(WF): Setting to null will allow the garbage collector the option of removing them from memory. It is up to the garbage collector whether it does so or not.
(STARTIGNORE)
(Hint):
(Subject): Java Essentials Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): If you are going to be working with foreign exchange rate application and need a very high amount of precision when working with currency, which type would be the best choice?
(A): float.
(B): double.
(C): Currency.
(D): BigInteger.
(E): BigDecimal.
(Correct): E
(Points): 1
(CF): BigDecimal would be best suited for maintaining precision while working with currency values.
(WF): BigDecimal would be best suited for maintaining precision while working with currency values.
(STARTIGNORE)
(Hint):
(Subject): Java Essentials Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): To compile the Java class SuperCoolApp which is stored in the package this.is.awesome, what should you type from the command line (in the project's root folder)?
(A): java this.is.awesome.SuperCoolApp
(B): javac this.is.awesome.SuperCoolApp.java
(C): javac SuperCoolApp.java
(D): java SuperCoolApp
(E): javac this\is\awesome\SuperCoolApp.java
(Correct): E
(Points): 1
(CF): E is the correct answer. A is how you would run the app after compiling. The other answers will not find the file.
(WF): E is the correct answer. A is how you would run the app after compiling. The other answers will not find the file.
(STARTIGNORE)
(Hint):
(Subject): Java Essentials Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): A method signature includes:
(A): The method's name.
(B): The class that contains the method.
(C): The method's return type.
(D): The method's parameters.
(E): The number of lines of code within the method.
(Correct): A, D
(Points): 1
(CF): A method's signature is its name and the arguments it accepts.
(WF): A method's signature is its name and the arguments it accepts.
(STARTIGNORE)
(Hint):
(Subject): Java Essentials Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 1 
(Grade style): 0
(Random answers): 1
(Question): In Java all variables must have their type declared.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Java is statically typed, meaning all variables must have their type declared.
(WF): Java is statically typed, meaning all variables must have their type declared.
(STARTIGNORE)
(Hint):
(Subject): Java Essentials Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What are some advantages of encapsulation in Java?
(A): Programs can be run on most platforms.
(B): Code can be put into small, maintainable units.
(C): Multiple inheritance becomes possible.
(D): You can group functions and data together.
(E): It supports the testing of software at the granular level.
(Correct): B, D, E
(Points): 1
(CF): A and C are not related to encapsulation. Multiple inheritance is also not possible in Java.
(WF): A and C are not related to encapsulation. Multiple inheritance is also not possible in Java.
(STARTIGNORE)
(Hint):
(Subject): Java Essentials Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)
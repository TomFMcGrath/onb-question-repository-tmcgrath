==========
Objective: Write jQuery code to get one or more elements using a variety of selectors.
==========

[[
Given an HTML data table:

<pre>
&LT;table id='dataTbl'&GT;
  &LT;tbody&GT;
    &LT;tr&GT; &LT;td&GT;...&LT;/td&GT; ... &LT;/tr&GT;
    &LT;tr&GT; &LT;td&GT;...&LT;/td&GT; ... &LT;/tr&GT;
  ...
  &LT;/tbody&GT;
&LT;/table&GT;
</pre>

Which two jQuery selectors will return each odd row in this specific table
and <em>only</em> in this table?  (Choose two)
]]
1: <code>$('table#dataTbl tr:odd')</code>
2: <code>$('#dataTbl').select('tr:odd')</code>
3: <code>$('#dataTbl').filter('tr:odd')</code>
*4: <code>$('table#dataTbl &GT; tbody &GT; tr:odd')</code>
5: <code>$('#dataTbl &GT; tbody').select('tr:odd')</code>
*6: <code>$('#dataTbl &GT; tbody').filter('tr:odd')</code>

[[SCENARIO DETAILS
<table id='dataTbl'>
  <tr> <td>...</td> ... </tr>
  <tr> <td>...</td> ... </tr>
  ...
</table>
]]


==========
Objective: Write jQuery code to manipulate the DOM.
==========

[[
You are building a web page that provides a glossary of terms.  The source HTML
merely includes the skeleton of a definition list:

<pre>&LT;dl id='glossaryList'&GT;&LT;/dl&GT;</pre>

Furthermore, a service provides a JSON map of terms to definitions like this:

<pre>var termMap = { 'Java' : 'An enterprise-scale programming language',
		  'JavaScript' : 'The standard programming language of the web',
		  'XML' : 'A data exchange language' };</pre>

Which jQuery code creates the DOM elements to populate the definition list?
]]
*1: <pre>function populateGlossary(termMap) {
	var $dl = $('#glossaryList');
	for (var term in termMap) {
		$dl.append('<dt>' + term + '</dt>');
		$dl.append('<dd>' + termMap[term] + '</dd>');
	}
}</pre>
2: <pre>function populateGlossary(termMap) {
	var $dl = $('#glossaryList');
	for (var term in termMap) {
		$dl.append($.createNode('dt'), term);
		$dl.append($.createNode('dd'), termMap[term]);
	}
}</pre>
3: <pre>function populateGlossary(termMap) {
	var $dl = $('#glossaryList');
	for (var term in termMap) {
		$dl.appendChild('<dt>' + term + '</dt>');
		$dl.appendChild('<dd>' + termMap[term] + '</dd>');
	}
}</pre>
4: <pre>function populateGlossary(termMap) {
	var $dl = $('#glossaryList');
	for (var term in termMap) {
		$dl.appendChild($.createNode('dt'), term);
		$dl.appendChild($.createNode('dd'), termMap[term]);
	}
}</pre>


==========
Objective: Write jQuery code to invoke an Ajax request.
==========

[[
You are building a Web 2.0 application that generates Ajax requests to query
the server for data periodically.  You have an icon in a special location that
is used to let the user know when a server request is being processed:

<pre>&LT;img id='waitImg' src='wait-indicator.png' style='display:none' /&GT;</pre>

Which jQuery code will toggle this image whenever any Ajax request is sent and received?
]]
1: <pre>$('#waitImg')
   .bind('ajaxSend', function() { $(this).show(); })
   .bind('ajaxRecv', function() { $(this).hide(); })</pre>
*2: <pre>$('#waitImg')
   .bind('ajaxSend', function() { $(this).show(); })
   .bind('ajaxComplete', function() { $(this).hide(); })</pre>
3: <pre>$('img.waitImg')
   .bind('ajaxSend', function() { $(this).setVisible(true); })
   .bind('ajaxRecv', function() { $(this).setVisible(false); })</pre>
4: <pre>$('img.waitImg')
   .bind('ajaxSend', function() { $(this).setVisible(true); })
   .bind('ajaxComplete', function() { $(this).setVisible(false); })</pre>

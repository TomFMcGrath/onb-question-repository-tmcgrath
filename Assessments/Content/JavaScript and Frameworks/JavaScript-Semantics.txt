==========
Objective: Write constructor function
==========

[[
You need to write a JavaScript constructor function that makes the object
immutable.

Which idiom guarantees that the object's data is unchangeable?
]]
1: <pre>function MyClass(value) {
   this._value = value;
}</pre>
*2: <pre>function MyClass(value) {
   this.getValue = function() { return value; }
}</pre>
3: <pre>function MyClass(value) {
   this.prototype.getValue = function() { return value; }
}</pre>
4: <pre>function MyClass(value) {
   MyClass.prototype.getValue = function() { return value; }
}</pre>


==========
Objective: Write code to create external methods
==========

[[
You are creating a <code>Circle</code> JavaScript class that contains a single
instance variable, <code>radius</code>.

Which chunk of code will create a method to calculate the area of the circle?
]]
1: <pre>Circle.getArea(this) {
   return 2 * Math.PI * this.radius * this.radius;
}</pre>
2: <pre>Circle.getArea = function() {
   return 2 * Math.PI * this.radius * this.radius;
}</pre>
*3: <pre>Circle.prototype.getArea = function() {
   return 2 * Math.PI * this.radius * this.radius;
}</pre>
4: <pre>class Circle {
   double getArea() {
      return 2 * Math.PI * this.radius * this.radius;
   }
}</pre>


==========
Objective: Write code to prevent a constructor function being called w/o the new keyword
==========

[[
You are creating a JavaScript constructor function for a <code>Circle</code>
class that contains a single instance variable, <code>radius</code>.

<pre>function Circle(r) {
   // ADD CODE HERE
   this.radius = r;
}<pre>

Which code snippet added to the constructor function will prevent the
constructor function from being invoked without the <code>new</code> operator.
]]
1: <code>if (this == null) throw "bad ctor invocation";</code>
*2: <code>if (this === window) throw "bad ctor invocation";</code>
3: <code>if (this === undefined) throw "bad ctor invocation";</code>
4: <code>if (typeof(this) !== 'object') throw "bad ctor invocation";</code>


==========
Objective: Explain the JS inheritance model: prototypal inheritance
==========

[[
Which type of object orientation does JavaScript support?
]]
1: objects with single inheritance
2: objects with multiple inheritance
3: object orientation is not supported
*4: objects with prototypal inheritance
5: objects with a single class, but no inheritance
